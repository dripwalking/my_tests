/*
* TASK #7
*/
/** 
 * Returns reversed copy of list
 * Input limits: 1 < list size < 100000000 (10**8)
 * Implementation requirements: Implementation should reverse list in SINGLE PASS.
 * @param {object} list - List object
 * @returns {object} reversedList - reversed copy of list
 */

const List = (head, tail=null) => ({ value: head, next: tail});

const list = List(5,
    List(7,
        List(12,
            List(45)
        )
    )
);

const reversed_list = reverse(list);

function reverse(src) {
   dst = List(src.value, null)
   src = src.next
   while (src) {
      dst = List(src.value, dst)
      src = src.next
   }
   return dst
}

console.log(reversed_list);

/*
* TASK #8
*/
/** 
 * Returns n-th fibonacci number. 1-st and 2-nd fibonacci numbers equal to 1
 * by the definition. Also for any integer n: F (n) = F(n-1)+F(n-2) 
 * where F (n) is n-th fibonacci number.
 * Input limits: -100 < n < 100
 * Implementation requirements: Implementation execution time should be LESS than 
 * 100ms on any input.
 * @param {number} n - index of fibonacci number to compute
 * @returns {number} - n-th fibonacci number
*/


cache = {}

var n = 10;
const result = fibb(n);

function _fibb(x){
  if (x in cache){
    return cache[x]
  } else {
    v = x <= 1 ? 1 : (fibb(x - 1) + fibb(x - 2))
    cache[x] = v
    return v
  }
}

function fibb(x){
    return x < 0 ? -_fibb(-x) : _fibb(x)
}

console.log(cache);